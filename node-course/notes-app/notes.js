const fs = require('fs');
const chalk = require('chalk')

debugger;
const getNotes = () => {
    return 'Your Notes...';
}

const addNote = (title,body) => {
    const notes = loadNotes();
    // const duplicateNotes = notes.filter( (note) => note.title === title);    
    const duplicateNote = notes.find( (note) => note.title === title );
    if(!duplicateNote){
        notes.push({
            title : title,
            body: body
        });    
        saveNotes(notes);
        console.log(chalk.green('New note added!'));
    }else{
        console.log(chalk.red('Note title taken!'));
    }
}

const readNote = (title) => {
    const notes = loadNotes();
    const note = notes.find( (note) => note.title === title);
    if(note){
        console.log('title : ' + note.title);
        console.log('Body : ' + note.body);
    }else{
        console.log(chalk.red('No Note found'));
    }
}


const saveNotes = (notes) => {
    const dataJSON =JSON.stringify(notes);
    fs.writeFileSync('notes.json',dataJSON);
}

const loadNotes = () => {
    try{
        const dataBuffer = fs.readFileSync('notes.json');
        const dataJSON = dataBuffer.toString();
        return JSON.parse(dataJSON);
    }catch(e){
        return [];
    }
}

const removeNotes = (title) => {
    const notes = loadNotes();
    const notesToKeep = notes.filter( (note) =>  note.title !== title );
    
    if(notesToKeep.length === notes.length)
        console.log(chalk.red('No notes found!'));
    else
        console.log(chalk.green('Note removed!'));
   saveNotes(notesToKeep);
}

const listNotes = () => {
    const notes = loadNotes();
    console.log(chalk.bold('Your Notes'));
    console.log(notes);
    notes.forEach( (note) => {
        // console.log ('title: ' + note.title + ' body: ' + note.body );
        console.log(note.title);
    })
}

module.exports = {
    getNotes: getNotes,
    removeNotes: removeNotes,
    addNote: addNote,
    listNotes: listNotes,
    readNote: readNote
}