
//console.log(process.argv[2]);
const yargs = require('yargs');
const chalk = require('chalk');
const notes =require('./notes');
const command = process.argv[2];


// if (command === 'add'){
//     console.log('Adding note!');
// } else if (command === 'remove') {
//     console.log('Removeing note!');
// }

// console.log(process.argv);
// console.log(yargs.argv);

//yargs.version('1.1.0');


yargs.command({
    command : 'add',
    description : 'Add a new note',
    builder: {
        title: {
            describe: 'Note title',
            demandOption : true,
            type:'string'
        },
        body: {
            description: 'Note body',
            demandOption: true,
            type: 'string'
        }
    },
    handler(argv) {
        //console.log('adding a new note',argv);
        // console.log('Title: ' + argv.title);
        // console.log('body: ' + argv.body);
        notes.addNote(argv.title, argv.body);
    }
});

yargs.command({
    command: 'remove',
    description: 'Remove a note',
    builder: {
        title: {
            describe: 'Note title',
            demandOption: true,
            type: 'string'
        }
    }, 
    handler(argv) {
        //console.log('Removing the note');
        notes.removeNotes(argv.title);
    }
})

yargs.command({
    command : 'list',
    description : 'listing a note',
    handler() {
        // console.log('Listing out all notes');
        notes.listNotes();
    }
});

yargs.command({
    command : 'read',
    description : 'Read a note',
    builder: {
        title: {
            discribe: 'Note Title',
            demandOption: true,
            type: 'string'
        }
    },
    handler(argv) {
        //console.log('reading a note');
        notes.readNote(argv.title);
    }
});
//VDO 16

//console.log(yargs.argv);

yargs.parse();


